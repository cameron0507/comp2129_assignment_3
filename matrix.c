#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <math.h>

#include "matrix.h"

static int g_seed = 0;

static int g_width = 0;
static int g_height = 0;
static int g_elements = 0;

static int g_nthreads = 1;

////////////////////////////////
///     UTILITY FUNCTIONS    ///
////////////////////////////////

/**
 * Returns pseudorandom number determined by the seed.
 */
int fast_rand(void) {

	g_seed = (214013 * g_seed + 2531011);
	return (g_seed >> 16) & 0x7FFF;
}

/**
 * Sets the seed used when generating pseudorandom numbers.
 */
void set_seed(int seed) {

	g_seed = seed;
}

/**
 * Sets the number of threads available.
 */
void set_nthreads(ssize_t count) {

	g_nthreads = count;
}

/**
 * Sets the dimensions of the matrix.
 */
void set_dimensions(ssize_t order) {

	g_width = order;
	g_height = order;

	g_elements = g_width * g_height;
}

/**
 * Displays given matrix.
 */
void display(const float* matrix) {

	for (ssize_t y = 0; y < g_height; y++) {
		for (ssize_t x = 0; x < g_width; x++) {
			if (x > 0) printf(" ");
			printf("%.2f", matrix[y * g_width + x]);
		}

		printf("\n");
	}
}

/**
 * Displays given matrix row.
 */
void display_row(const float* matrix, ssize_t row) {

	for (ssize_t x = 0; x < g_width; x++) {
		if (x > 0) printf(" ");
		printf("%.2f", matrix[row * g_width + x]);
	}

	printf("\n");
}

/**
 * Displays given matrix column.
 */
void display_column(const float* matrix, ssize_t column) {

	for (ssize_t i = 0; i < g_height; i++) {
		printf("%.2f\n", matrix[i * g_width + column]);
	}
}

/**
 * Displays the value stored at the given element index.
 */
void display_element(const float* matrix, ssize_t row, ssize_t column) {

	printf("%.2f\n", matrix[row * g_width + column]);
}

////////////////////////////////
///   MATRIX INITALISATIONS  ///
////////////////////////////////

/**
 * Returns new matrix with all elements set to zero.
 */
float* new_matrix(void) {

	return calloc(g_elements, sizeof(float));
}

/**
 * Returns new identity matrix.
 */
float* identity_matrix(void) {

	float* result = new_matrix();

	for (int i = 0; i < g_width; i++) {
		result[i * g_width + i] = 1;
	}

	return result;
}

/**
 * Returns new matrix with elements generated at random using given seed.
 */
float* random_matrix(int seed) {

	float* result = new_matrix();

	set_seed(seed);

	for (ssize_t i = 0; i < g_elements; i++) {
		result[i] = fast_rand();
	}

	return result;
}

/**
 * Returns new matrix with all elements set to given value.
 */
float* uniform_matrix(float value) {

	float* result = new_matrix();

	for (int i = 0; i < g_elements; i++) {
		result[i] = value;
	}

	return result;
}

/**
 * Returns new matrix with elements in sequence from given start and step
 */
float* sequence_matrix(float start, float step) {

	float* result = new_matrix();

	for (int i = 0; i < g_elements; i++) {
		result[i] = start;
		start += step;
	}

	return result;
}

////////////////////////////////
///     MATRIX OPERATIONS    ///
////////////////////////////////

/**
 * Returns new matrix with elements cloned from given matrix.
 */
float* cloned(const float* matrix) {

	float* result = new_matrix();

	for (ssize_t y = 0; y < g_height; y++) {
		for (ssize_t x = 0; x < g_width; x++) {
			result[y * g_width + x] = matrix[y * g_width + x];
		}
	}

	return result;
}

/**
 * Returns new matrix with elements in ascending order.
 */
int compare (const void *a, const void *b) {
  const float *n_a = (const float *) a;
  const float *n_b = (const float *) b;

  return (*n_a > *n_b) - (*n_a < *n_b);
}

float* sorted(const float* matrix) {

	float* result = new_matrix();

	for (int i = 0; i < g_elements; i++) {
		result[i] = matrix[i];
	}

	qsort(result, g_elements, sizeof(float), compare);

	return result;
}

/**
 * Returns new matrix with elements rotated 90 degrees clockwise.
 */
float* rotated(const float* matrix) {

	float* result = new_matrix();

	for (int i = 0; i < g_width; i++) {
		for (int j = 0; j < g_width; j++) {
			result[((g_width - i) + ((g_width) * j)) - 1] = matrix[g_width * i + j];
		}
	}

	return result;
}

/**
 * Returns new matrix with elements ordered in reverse.
 */
float* reversed(const float* matrix) {

	float* result = new_matrix();

	for (int i = 0; i < g_elements; i++) {
		result[i] = matrix[g_elements - 1 - i];
	}

	return result;
}

/**
 * Returns new transposed matrix.
 */
float* transposed(const float* matrix) {

	float* result = new_matrix();

	for (int i = 0; i < g_width; i++) {
		for (int j = 0; j < g_width; j++) {
			result[j * g_width + i] = matrix[i * g_width + j];
		}
	}

	return result;
}

/**
 * Returns new matrix with scalar added to each element.
 */
float* scalar_add(const float* matrix, float scalar) {

	float* result = new_matrix();

	for (int i = 0; i < g_elements; i++) {
		result[i] = matrix[i] + scalar;
	}

	return result;
}

/**
 * Returns new matrix with scalar multiplied to each element.
 */
float* scalar_mul(const float* matrix, float scalar) {

	float* result = new_matrix();

	for (int i = 0; i < g_elements; i++) {
		result[i] = matrix[i] * scalar;
	}

	return result;
}

/**
 * Returns new matrix that is the result of
 * adding the two given matrices together.
 */
float* matrix_add(const float* matrix_a, const float* matrix_b) {

	float* result = new_matrix();

	for (int i = 0; i < g_elements; i++) {
		result[i] = matrix_a[i] + matrix_b[i];
	}

	return result;
}

/**
 * Returns new matrix that is the result of
 * multiplying the two matrices together.
 */
float* matrix_mul(const float* matrix_a, const float* matrix_b) {

	float* result = new_matrix();
	float tmp[g_elements];

	for (int i = 0; i < g_width; i++) {
		for (int j = 0; j < g_width; j++) {
			tmp[i*g_width + j] = matrix_b[j*g_width + i];
		}
	}
	for (int i = 0; i < g_width; i++) {
		for (int j = 0; j < g_width; j++) {
			float sum = 0.0;
			for (int k = 0; k < g_width; k++) {;
				sum += matrix_a[i*g_width + k] * tmp[j * g_width + k];
			}
			result[i*g_width + j] = sum;
		}
	}
	return result;

}

/**
 * Returns new matrix that is the result of
 * powering the given matrix to the exponent.
 */
float* matrix_pow(const float* matrix, int exponent) {

	// Check special case of a zero exponent.
	if (!exponent) {
		return identity_matrix();
	}

	float* result;
	float* worker = new_matrix();

	for (int i = 0; i < g_elements; i++) {
		worker[i] = matrix[i];
	}

	for (int i = 0; i < exponent - 1; i++) {
		result = matrix_mul(worker, matrix);
		for (int j = 0; j < g_elements; j++) {
			worker[j] = result[j];
		}
		free(result);
	}

	return worker;
}

/**
 * Returns new matrix that is the result of
 * convolving given matrix with a 3x3 kernel matrix.
 */
float* matrix_conv(const float* matrix, const float* kernel) {

	float* result = new_matrix();

	/*

		Convolution is the process in which the values of a matrix are
		computed according to the weighted sum of each value and it's
		neighbours, where the weights are given by the kernel matrix.

		01 02 03 04			-4 -2 -1  1
		05 06 07 08  ->      4  6  7  9
		09 10 11 12			 8 10 11 13
		13 14 15 16			16 18 19 21
	*/
	// For each row in matrix
	for (int i = 0; i < g_width; i++) {
		// For each column in matrix
		for (int j = 0; j < g_width; j++) {
			float sum = 0;

			// 9 sums to make
			int no_left = 0;
			int no_right = 0;
			int no_up = 0;
			int no_down = 0;

			no_left = j == 0? 1: 0;
			no_right = j == g_width - 1? 1: 0;
			no_up = i == 0? 1: 0;
			no_down = i == g_width - 1? 1: 0;

			// kernel 0.
			if (no_left) {
				if (no_up) {
					sum += kernel[0]*matrix[(i)*g_width + (j)];
				} else {
					sum += kernel[0]*matrix[(i-1)*g_width + (j)];
				}
			} else if (no_up) {
				sum += kernel[0]*matrix[(i)*g_width + (j-1)];
			} else {
				sum += kernel[0]*matrix[(i-1)*g_width + (j-1)];
			}

			// kernel 1.
			sum += no_up? kernel[1]*matrix[i*g_width + j]: kernel[1]*matrix[(i-1)*g_width + j];

			// kernel 2.
			if (no_right) {
				if (no_up) {
					sum += kernel[2]*matrix[(i)*g_width + (j)];
				} else {
					sum += kernel[2]*matrix[(i-1)*g_width + (j)];
				}
			} else if (no_up) {
				sum += kernel[2]*matrix[(i)*g_width + (j+1)];
			} else {
				sum += kernel[2]*matrix[(i-1)*g_width + (j+1)];
			}

			// kernel 3.
			sum += no_left? kernel[3]*matrix[i*g_width + j]: kernel[3]*matrix[i*g_width + (j-1)];

			// kernel 4.
			sum += kernel[4]*matrix[i*g_width + j];

			// kernel 5.
			sum += no_right? kernel[5]*matrix[i*g_width + j]: kernel[5]*matrix[i*g_width + (j+1)];

			// kernel 6.
			if (no_left) {
				if (no_down) {
					sum += kernel[6]*matrix[(i)*g_width + (j)];
				} else {
					sum += kernel[6]*matrix[(i+1)*g_width + (j)];
				}
			} else if (no_down) {
				sum += kernel[6]*matrix[(i)*g_width + (j-1)];
			} else {
				sum += kernel[6]*matrix[(i+1)*g_width + (j-1)];
			}

			// kernel 7.
			sum += no_down? kernel[7]*matrix[i*g_width + j]: kernel[7]*matrix[(i+1)*g_width + j];

			// kernel 8.
			if (no_right) {
				if (no_down) {
					sum += kernel[8]*matrix[(i)*g_width + (j)];
				} else {
					sum += kernel[8]*matrix[(i+1)*g_width + (j)];
				}
			} else if (no_down) {
				sum += kernel[8]*matrix[(i)*g_width + (j+1)];
			} else {
				sum += kernel[8]*matrix[(i+1)*g_width + (j+1)];
			}


			result[i*g_width + j] = sum;
		}
	}

	return result;
}

////////////////////////////////
///       COMPUTATIONS       ///
////////////////////////////////

/**
 * Returns the sum of all elements.
 */
float get_sum(const float* matrix) {

	float result = 0;
	for (int i = 0; i < g_elements; i++) {
		result += matrix[i];
	}

	return result;
}

/**
 * Returns the trace of the matrix.
 */
float get_trace(const float* matrix) {

	float count = 0.0;
	for (int i = 0; i < g_width; i++) {
		count += matrix[i*g_width + i];
	}

	return count;
}

/**
 * Returns the smallest value in the matrix.
 */
float get_minimum(const float* matrix) {

	float min = matrix[0];
	for (int i = 0; i < g_elements; i++) {
		if (matrix[i] < min) {
			min = matrix[i];
		}
	}

	return min;
}

/**
 * Returns the largest value in the matrix.
 */
float get_maximum(const float* matrix) {

	float max = matrix[0];
	for (int i = 0; i < g_elements; i++) {
		if (matrix[i] > max) {
			max = matrix[i];
		}
	}

	return max;
}

/**
 * Returns the determinant of the matrix.
 */
float get_determinant(const float* matrix) {

	float lower[g_elements];
	float upper[g_elements];

	int i, j, k;
	for (j = 0; j < g_width; j++) {
        for (i = 0; i < g_width; i++) {
            if (i <= j) {
                upper[i*g_width + j] = matrix[i*g_width + j];
                for (k = 0; k <= i - 1; k++) {
                    upper[i*g_width + j] -= lower[i*g_width + k] * upper[k*g_width + j];
				}
                if (i == j) {
                    lower[i*g_width + j] = 1;
                } else {
                    lower[i*g_width + j] = 0;
				}
            } else {
                lower[i*g_width + j] = matrix[i*g_width + j];
                for (k = 0; k <= j - 1; k++) {
                    lower[i*g_width + j] -= lower[i*g_width + k] * upper[k*g_width + j];
				}
                lower[i*g_width + j] /= upper[j*g_width + j];
                upper[i*g_width + j] = 0;
            }
        }
    }

	// Then, hopefully, the LU decomposition for the given matrix is complete.
	//	Now, the deterinant of the matrix should be able to be calculated.
	float lower_determinant = lower[0];
	float upper_determinant = upper[0];
	for (int i = 1; i < g_width; i++) {
		lower_determinant = lower_determinant * lower[i * g_width + i];
		upper_determinant = upper_determinant * upper[i * g_width + i];
	}
	float determinant = lower_determinant * upper_determinant;
	if (isnan(determinant)) {
		return 0;
	}
	return determinant;
}

/**
 * Returns the frequency of the given value in the matrix.
 */
int get_frequency(const float* matrix, float value) {

	int count = 0;
	for (int i = 0; i < g_elements; i++) {
		if (matrix[i] == value) {
			count++;
		}
	}

	return count;
}
